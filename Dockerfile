FROM maven:3.8.5-openjdk-17 AS build
RUN mkdir /project
COPY src /project/src
COPY pom.xml /project
RUN mvn -f /project/pom.xml package -DskipTests

FROM khipu/openjdk17-alpine:latest
RUN mkdir /app
COPY --from=build /project/target/e-commerce-be-0.0.1-SNAPSHOT.jar /app/e-commerce-be.jar
WORKDIR /app
CMD java $JAVA_OPTS -jar e-commerce-be.jar

