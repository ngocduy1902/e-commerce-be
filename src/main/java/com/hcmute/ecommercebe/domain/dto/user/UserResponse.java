package com.hcmute.ecommercebe.domain.dto.user;

import com.hcmute.ecommercebe.domain.entity.User;
import com.hcmute.ecommercebe.domain.enums.UserRole;
import lombok.Data;

@Data
public class UserResponse {
    private String id;
    private String email;
    private String username;
    private String fullName;
    private UserRole role;
    private boolean status;

    public UserResponse(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.username = user.getUsername();
        this.fullName = user.getFullName();
        this.role = user.getRole();
        this.status = user.isStatus();
    }
}
