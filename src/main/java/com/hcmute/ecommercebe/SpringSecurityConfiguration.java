package com.hcmute.ecommercebe;


import com.hcmute.ecommercebe.domain.entity.User;
import com.hcmute.ecommercebe.domain.enums.UserRole;
import com.hcmute.ecommercebe.jwt.JwtAuthenticationFilter;
import com.hcmute.ecommercebe.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfiguration {

    @Autowired
    @Lazy
    private AuthenticationProvider authenticationProvider;
    @Autowired
    @Lazy
    private UserRepo userRepo;

    @Autowired
    @Lazy
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http, HandlerMappingIntrospector introspector) throws Exception {
        MvcRequestMatcher.Builder mvcMatcherBuilder = new MvcRequestMatcher.Builder(introspector);
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .authorizeHttpRequests(rq -> rq
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/admin/**")).hasAuthority(UserRole.ADMIN.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/user/**")).authenticated()
                        .requestMatchers(mvcMatcherBuilder.pattern("/**")).permitAll())
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                User userEntity = userRepo.getUserByUsername(username);
                if (userEntity == null) {
                    throw new UsernameNotFoundException("Not user with username = " + username);
                }
                return userEntity;
            }
        };
    }

    @Bean
    protected AuthenticationProvider authenticationProvider() {
        return this.authenticationProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    protected BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
