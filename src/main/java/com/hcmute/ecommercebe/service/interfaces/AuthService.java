package com.hcmute.ecommercebe.service.interfaces;

import com.hcmute.ecommercebe.domain.base.ResponseBaseAbstract;
import com.hcmute.ecommercebe.domain.dto.auth.LoginRequest;
import com.hcmute.ecommercebe.domain.dto.auth.RegisterRequest;

public interface AuthService {
    ResponseBaseAbstract login(LoginRequest loginRequest);

    ResponseBaseAbstract register(RegisterRequest registerRequest);
}
