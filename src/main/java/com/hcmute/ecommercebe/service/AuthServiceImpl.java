package com.hcmute.ecommercebe.service;

import com.hcmute.ecommercebe.domain.base.ResponseBaseAbstract;
import com.hcmute.ecommercebe.domain.base.SuccessResponse;
import com.hcmute.ecommercebe.domain.dto.auth.LoginRequest;
import com.hcmute.ecommercebe.domain.dto.auth.RegisterRequest;
import com.hcmute.ecommercebe.domain.dto.user.UserResponse;
import com.hcmute.ecommercebe.domain.entity.User;
import com.hcmute.ecommercebe.domain.enums.UserRole;
import com.hcmute.ecommercebe.domain.exception.ServiceExceptionFactory;
import com.hcmute.ecommercebe.jwt.JwtTokenProvider;
import com.hcmute.ecommercebe.repository.UserRepo;
import com.hcmute.ecommercebe.service.interfaces.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public ResponseBaseAbstract login(LoginRequest loginRequest) {
        User user = this.userRepo.getUserByUsername(loginRequest.getUsername());

        if (user == null) {
            throw ServiceExceptionFactory.unauthorized().setMessage("Username không tồn tại");
        }

        if (!this.passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())) {
            throw ServiceExceptionFactory.unauthorized().setMessage("Mật khẩu không đúng");
        }

        SuccessResponse response = new SuccessResponse();
        response.setMessage("Đăng nhập thành công");
        Map<String, Object> data = new HashMap<>();
        data.put("user",user);
        data.put("accessToken",this.jwtTokenProvider.generateToken(user));
        response.setData(data);
        return response;
    }

    @Override
    public ResponseBaseAbstract register(RegisterRequest registerRequest) {
        if (this.userRepo.existByUsername(registerRequest.getUsername()))
            throw ServiceExceptionFactory.duplicate().setMessage("Username đã tồn tại");

        User user = new User();
        user.setEmail(registerRequest.getEmail());
        user.setUsername(registerRequest.getUsername());
        user.setPassword(this.passwordEncoder.encode(registerRequest.getPassword()));
        user.setFullName(registerRequest.getFullName());
        user.setRole(UserRole.USER);
        user.setStatus(true);
        this.userRepo.save(user);

        SuccessResponse response = new SuccessResponse();
        response.setData(new UserResponse(user));
        response.setMessage("Đăng ký thành công");
        return response;
    }
}
