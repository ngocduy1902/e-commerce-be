package com.hcmute.ecommercebe.controller.auth;

import com.hcmute.ecommercebe.domain.base.ResponseBaseAbstract;
import com.hcmute.ecommercebe.domain.dto.auth.RegisterRequest;
import com.hcmute.ecommercebe.service.interfaces.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/auth/register")
public class RegisterController {
    @Autowired
    private AuthService authService;

    @PostMapping("")
    public ResponseBaseAbstract register(@RequestBody RegisterRequest registerRequest) {
        return this.authService.register(registerRequest);
    }
}
